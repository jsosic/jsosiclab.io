---
layout: page
title: About
permalink: /about/
---

I am freelance devops and infrastructure engineer, currently working for [Toptal](www.toptal.com) in Core infrastructure team.

Living and working from Split, Croatia.

You are welcome to [email](mailto: jakov.sosic@gmail.com) me.
