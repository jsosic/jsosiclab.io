FROM ruby:2.3.2
MAINTAINER Jakov Sosic "<jsosic@gmail.com>"
WORKDIR /blog
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .
CMD ["bundle", "exec", "jekyll", "build", "-d", "_site"]
VOLUME ['./blog']
