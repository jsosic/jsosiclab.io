![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

## jsosic blog

> This project forked and has been modified from:
* [My Stack Problems](https://github.com/agusmakmun/agusmakmun.github.io)
* [A simple grey theme for Jekyll](https://github.com/liamsymonds/simplygrey-jekyll)
* [Super Search](https://github.com/chinchang/super-search)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Generate the website: `jekyll build -d public`
1. Preview your project: `jekyll serve`
1. Add content

Read more at Jekyll's [documentation](https://jekyllrb.com/docs/home/).

### Demo
* [https://jsosic.gitlab.io](https://jsosic.gitlab.io)


#### Feature

* Sitemap and XML Feed
* Paginations in homepage
* Posts under category
* Realtime Search Posts _(title & description)_ under query.
* Related Posts
* Highlight pre
* Next & Previous Post
* Disqus comment
* Projects
* Project page
* Share on social media
* Google analytics
