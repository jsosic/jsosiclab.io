#!/bin/bash -x

# exit early on any error
set -e

docker --version

docker build -t docker.io/jsosic/blog:latest .

[[ ! -d _site ]] && mkdir _site
# use local uid and gid so that docker won't write to volume as root
docker run --privileged=true -u "$(id -u):$(id -g)" --rm -v $PWD/_site:/blog/_site:z docker.io/jsosic/blog:latest
